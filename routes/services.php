<?php
        
// Start Routes for mkebun 
Route::resource('services/mkebun','Services\MkebunController');
// End Routes for mkebun 

                    
// Start Routes for nilaiekonomistan 
Route::resource('services/nilaiekonomistan','Services\NilaiekonomistanController');
// End Routes for nilaiekonomistan 

                    
// Start Routes for affdelingmaster 
Route::resource('services/affdelingmaster','Services\AffdelingmasterController');
// End Routes for affdelingmaster 

                    
// Start Routes for blokmaster 
Route::resource('services/blokmaster','Services\BlokmasterController');
// End Routes for blokmaster 

                    
// Start Routes for jenistanaman 
Route::resource('services/jenistanaman','Services\JenistanamanController');
// End Routes for jenistanaman 

                    
// Start Routes for mutasi 
Route::resource('services/mutasi','Services\MutasiController');
// End Routes for mutasi 

                    
// Start Routes for biodata 
Route::resource('services/biodata','Services\BiodataController');
// End Routes for biodata 

                    
// Start Routes for txtanaman 
Route::resource('services/txtanaman','Services\TxtanamanController');
// End Routes for txtanaman 

                    
// Start Routes for klasifikasitanaman 
Route::resource('services/klasifikasitanaman','Services\KlasifikasitanamanController');
// End Routes for klasifikasitanaman 

                    
// Start Routes for jabatanmaster 
Route::resource('services/jabatanmaster','Services\JabatanmasterController');
// End Routes for jabatanmaster 

                    
// Start Routes for ajuanmutasi 
Route::resource('services/ajuanmutasi','Services\AjuanmutasiController');
// End Routes for ajuanmutasi 

                    
// Start Routes for masterrkap 
Route::resource('services/masterrkap','Services\MasterrkapController');
// End Routes for masterrkap 

                    
// Start Routes for laporanmutasi 
Route::resource('services/laporanmutasi','Services\LaporanmutasiController');
// End Routes for laporanmutasi 

                    
// Start Routes for validasimutasi 
Route::resource('services/validasimutasi','Services\ValidasimutasiController');
// End Routes for validasimutasi 

                    ?>